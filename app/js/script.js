const searchBtn = document.querySelector('.search-button');

function findWord() {
  let searchInput = document.querySelector('.search-input');
  let searchWord = searchInput.value;

  const text = document.querySelector('.container');


  let wordMatch = document.querySelector('.word-match');

  let regExp = new RegExp(searchWord, 'i');

  if (regExp.exec(text.textContent) && searchWord !== '') {
    let reg = new RegExp(searchWord, 'gi');

    // console.log(searchWord);
    let match = text.textContent.match(reg).length;
    if (match !== 0) {
      wordMatch.innerHTML = `Количество совпадений: ${ match }`;
    }
    text.innerHTML = text.innerHTML.replace(reg, `<span class="hl">${ searchWord }</span>`);

  } else {
    wordMatch.innerHTML = 'Ничего не найдено!';

  }
}

searchBtn.addEventListener('click', findWord);



